package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class BoardsPage extends BasePage {

    public String pageUrl = "https://trello.com/%s/boards";

    public BoardsPage(WebDriver webdriver) {
        super(webdriver);
    }

    public WebElement getCreateBoardNaviButton(){
        return driver.findElement(By.cssSelector("div[class='board-tile mod-add']"));
    }

    public WebElement getCreateBoardDropDownButton(){
        return driver.findElement(By.xpath("//button[@data-test-id ='header-create-board-button']"));
    }

    public WebElement getboardNameInput(){
        return driver.findElement(By.xpath("//input[@data-test-id ='create-board-title-input']"));
    }

    public WebElement getCreateBoardButton(){
        return driver.findElement(By.xpath("//button[@data-test-id ='create-board-submit-button']"));
    }

    public List<WebElement> getAllBoardNames(){
        return driver.findElements(By.xpath("//li[@class='boards-page-board-section-list-item']//div[@class='board-tile-details-name']"));
    }

    public void CreateNewBoard(String boardName){
        getCreateBoardNaviButton().click();
        getboardNameInput().sendKeys(boardName);
        wait.until(ExpectedConditions.elementToBeClickable(getCreateBoardButton()));
        getCreateBoardButton().click();
    }

    public void AssertBoardCreated(String boardName){
        // Replacing all whitespaces with a dash to form expected URL
        wait.until(ExpectedConditions.urlContains(boardName.replaceAll("\\s+","-").toLowerCase()));
    }

    public void AssertNavigated(String username){
        // Formatting the expected URL to contain the username
        wait.until(ExpectedConditions.urlToBe(String.format(pageUrl, username)));
    }
}
