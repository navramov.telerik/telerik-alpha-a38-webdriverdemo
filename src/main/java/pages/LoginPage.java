package pages;

import dev.failsafe.internal.util.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

public class LoginPage extends BasePage {

    public String pageUrl = "https://trello.com/login";

    public LoginPage(WebDriver webdriver) {
        super(webdriver);
    }

    public WebElement getUser(){
        return driver.findElement(By.id("user"));
    }

    public WebElement getPassword(){
        return driver.findElement(By.id("password"));
    }

    public WebElement getAtlassianLoginButton(){
        return driver.findElement(By.id("login"));
    }

    public WebElement getLoginButton(){
        return driver.findElement(By.id("login-submit"));
    }

    public void authenticateUser(String username, String pass) throws InterruptedException {
        getUser().sendKeys(username);
        getAtlassianLoginButton().submit();
        Thread.sleep(1500);

        getPassword().sendKeys(pass);
        getLoginButton().submit();
    }

    public void WaitUntilNavigated(){
        wait.until(ExpectedConditions.urlToBe(pageUrl));
    }

    public void Navigate() {
        driver.get(pageUrl);
    }

    public void AssertUserAuthenticated(String firstName, String lastName, String username) {

        Assert.isTrue(driver.findElement(By.cssSelector(String.format("span[title='%s %s ($s)']", firstName, lastName, username))).isDisplayed(), "Avatar not found");
    }
}
