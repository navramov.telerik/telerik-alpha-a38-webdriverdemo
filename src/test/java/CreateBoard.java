import org.junit.Test;
import pages.BoardsPage;
import pages.LoginPage;
import java.time.LocalDateTime;

public class CreateBoard extends BaseTest{

    private String boardName = "Alpha 38 Tasks";

    @Test
    public void boardCreated_when_createBoardNavigationButtonClicked() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.Navigate();
        loginPage.authenticateUser("viray74681@agrolivana.com", "alpha2022");

        BoardsPage boardsPage = new BoardsPage(driver);
        boardsPage.AssertNavigated("viray74681");

        // Append current seconds to pre-defined expected string
        String uniqueBoardName = boardName + LocalDateTime.now().getSecond();

        boardsPage.CreateNewBoard(uniqueBoardName);

        boardsPage.AssertBoardCreated(uniqueBoardName);
    }
}
