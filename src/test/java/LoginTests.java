import dev.failsafe.internal.util.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.LoginPage;

public class LoginTests extends BaseTest{

    private static final String BOARD_NAME = "Alpha 38";

    @Test
    public void userAuthenticated_when_loginWithValidCredentials() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.authenticateUser("viray74681@agrolivana.com", "alpha2022");

        loginPage.WaitUntilNavigated();

        loginPage.AssertUserAuthenticated("Al", "Pha", "viray74681");
    }
}
